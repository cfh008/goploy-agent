#!/bin/bash

target_name=goploy-agent
src_dir=/root/download/middle/$target_name

# 如果外部传递了安装目录，则以外部传递为准
while getopts "i:" opt; do
  case $opt in
    i)
      src_dir=$OPTARG
      ;;
    \?)
      echo "Invalid option: -$OPTARG"
      ;;
  esac
done

# 先查看服务状态并停止服务
cmd_result=`systemctl status $target_name`
is_running=0
# echo "cmd result: $cmd_result"
result=`echo $cmd_result | grep "active (running)"`
if [[ "$result" != "" ]];then
	is_running=1
fi

if [ $is_running -ne 0 ];then
	echo "stop $target_name service"
	systemctl stop $target_name
fi

# 删除将要替换的文件，将把新的文件解压释放
if [ ! -d $src_dir ];then
	echo "$srcdir not exists!"
	exit 1
fi

dst_dir=/var/www/$target_name
if [ ! -d $dst_dir ];then
	mkdir -p $dst_dir
fi

exec_path=$dst_dir/$target_name
service_path=$dst_dir/$target_name.service
setuprun_path=$dst_dir/setuprun.sh
dist_path=$dst_dir/dist

if [ -f $exec_path ];then
	rm -f $exec_path
fi

if [ -f $service_path ];then
	rm -f $service_path
fi

if [ -f $setuprun_path ];then
	rm -f $setuprun_path
fi

if [ -d $dist_path ];then
	echo "remove origin dist files"
	rm -rf $dist_path
fi

config_file=$target_name.toml
src_config_path=$src_dir/$config_file
if [ ! -f $src_config_path ];then
	echo "$src_config_path not exists!"
	exit 1
fi

cp -f $src_config_path $dst_dir/

tar_file_name=$target_name.tar.gz
src_tar_path=$src_dir/$tar_file_name
if [ ! -f $src_tar_path ];then
	echo "$src_tar_path not exists!"
	exit 1
fi

cp -f $src_tar_path $dst_dir/
cd $dst_dir
echo "extract files from compressed file"
tar -zxvf ./$tar_file_name
rm -f $dst_dir/$tar_file_name

# 如果不存在服务，先安装配置服务
target_service_parent=/usr/lib/systemd/system
service_name=$target_name.service
target_service_path=$target_service_parent/$service_name
if [ ! -f $target_service_path ];then
	cp -f $dst_dir/$service_name $target_service_parent/
	systemctl enable $target_name
else
	cp -f $dst_dir/$service_name $target_service_parent/
fi

# 重新加载服务配置
systemctl daemon-reload

# 重新启动服务
systemctl start $target_name

echo "setup and start $target_name service success."

