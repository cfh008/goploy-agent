#!/bin/bash

# 获取当前脚本所在文件路径
curr_dir=$(cd `dirname $0`; pwd)
cd $curr_dir

exec_name=goploy-agent

version=""
to_commit=0
to_build_all_os=0
to_build_web=0
#echo begin $version
while getopts "v:caw" opt; do
  case $opt in
    v)
      version=$OPTARG
      ;;
    c)
      to_commit=1
      ;;
    a)
      to_build_all_os=1
      ;;
    w)
      to_build_web=1
      ;;
    \?)
      echo "Invalid option: -$OPTARG"
      ;;
  esac
done

main_file=main.go
web_file=web/package.json

if [[ $version =~ ^[1-9].[0-9].[0-9]$ ]]
then
  sed -i -e "s/const appVersion = \"[0-9].[0-9].[0-9]\"/const appVersion = \"$version\"/g" $main_file
  sed -i -e "s/\"version\": \"[0-9].[0-9].[0-9]\",/\"version\": \"$version\",/g" $web_file
fi

if [ $to_build_web -ne 0 ];then
  cd web
  npm run build
  cd ..
fi

echo "Building $exec_name";

if [ $to_build_all_os -eq 0 ];then
  env GOOS=linux go build -o $exec_name $main_file
else
  env GOOS=linux go build -o $exec_name $main_file
  env GOOS=darwin go build -o $exec_name.mac $main_file
  env GOOS=windows go build -o $exec_name.exe $main_file
fi

# 将需要的文件统一打包，方便解压运行
# 删除原本生成的中间目录
pack_name=pack
pack_middle_dir=$curr_dir/$pack_name
if [ -d $pack_middle_dir ];then
	rm -rf $pack_middle_dir
fi
mkdir $pack_middle_dir

src_dist_dir=$curr_dir/web/dist

if [ ! -d $src_dist_dir ];then
  echo "$src_dist_dir not exists!"
  exit 1
fi

cp -rf $src_dist_dir $pack_middle_dir/
service_name=$exec_name.service
setuprun_name=setuprun.sh
cp -f $curr_dir/$service_name $pack_middle_dir/
cp -f $curr_dir/$setuprun_name $pack_middle_dir/

# 依据参数值进行可执行文件拷贝操作
if [ $to_build_all_os -eq 0 ];then
  cp -f $curr_dir/$exec_name $pack_middle_dir/
  exec_info="$exec_name"
else
  cp -f $curr_dir/$exec_name $pack_middle_dir/
  cp -f $curr_dir/$exec_name.mac $pack_middle_dir/
  cp -f $curr_dir/$exec_name.exe $pack_middle_dir/
  exec_info="$exec_name $exec_name.mac $exec_name.exe"
fi


cd $pack_middle_dir
target_pack_path=$curr_dir/$exec_name.tar.gz
if [ -f $target_pack_path ];then
  rm -f $target_pack_path
fi

tar -zcvf $target_pack_path $service_name $setuprun_name $exec_info dist
cd $curr_dir
rm -rf $pack_middle_dir

if [ $to_commit -eq 0 ];then
  git checkout -- $main_file
  git checkout -- $web_file
  # echo "no commit"
else
  # echo "to commit"
  git add $main_file
  git add $web_file
  git commit -m"1.更新系统版本号。"
  git pull
  git push
fi